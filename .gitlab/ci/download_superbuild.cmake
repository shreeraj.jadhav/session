cmake_minimum_required(VERSION 3.12)

# In order to use a new superbuild, please copy the item from the date stamped
# directory into the `keep` directory. This ensure that new versions will not
# be removed by the uploader script and preserve them for debugging in the
# future.

set(data_host "https://data.kitware.com")

# Determine the tarball to download.
# 20220311 — Catch SMTK change: 3D widgets don't hide.
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2019")
  set(file_item "623c90a44acac99f42d95175")
  set(file_hash "76f7b28c745fff2fd6798173d58243da8ee43ee7fa85aca3570c5287ce5033df5a7da80b96fda26e3401298954b7e199c678e3b691f3309f47cf61a9907c7774")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos")
  set(file_item "623c90a34acac99f42d9516f")
  set(file_hash "3eb98d65ff2113943412aefb319af5277258f47f78957f3400ab7640f4142bcbddf9bce17f4db5bdfa95bab4886beb0da96f692f091a3c961ef91ac2172ecde5")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_item OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "${data_host}/api/v1/item/${file_item}/download"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
