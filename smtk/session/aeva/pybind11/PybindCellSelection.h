//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_aeva_CellSelection_h
#define pybind_smtk_session_aeva_CellSelection_h

#include <pybind11/pybind11.h>

#include "smtk/session/aeva/CellSelection.h"

namespace py = pybind11;

inline PySharedPtrClass< smtk::session::aeva::CellSelection, smtk::model::Entity > pybind11_init_smtk_session_aeva_CellSelection(py::module &m)
{
  PySharedPtrClass< smtk::session::aeva::CellSelection, smtk::model::Entity > instance(m, "CellSelection");
  instance
    .def(py::init<::std::shared_ptr<smtk::session::aeva::Resource> const &, ::vtkDataObject *>())
    .def(py::init<::smtk::session::aeva::CellSelection const &>())
    .def("deepcopy", (smtk::session::aeva::CellSelection & (smtk::session::aeva::CellSelection::*)(::smtk::session::aeva::CellSelection const &)) &smtk::session::aeva::CellSelection::operator=)
    .def("typeName", &smtk::session::aeva::CellSelection::typeName)
    .def("shared_from_this", (std::shared_ptr<smtk::session::aeva::CellSelection> (smtk::session::aeva::CellSelection::*)()) &smtk::session::aeva::CellSelection::shared_from_this)
    .def("shared_from_this", (std::shared_ptr<const smtk::session::aeva::CellSelection> (smtk::session::aeva::CellSelection::*)() const) &smtk::session::aeva::CellSelection::shared_from_this)
    .def("name", &smtk::session::aeva::CellSelection::name)
    .def("data", &smtk::session::aeva::CellSelection::data)
    .def("replaceData", &smtk::session::aeva::CellSelection::replaceData, py::arg("selectedCells"))
    .def_static("create", (std::shared_ptr<smtk::session::aeva::CellSelection> (*)(::std::shared_ptr<smtk::session::aeva::Resource> const &, ::vtkDataObject *, ::std::weak_ptr<smtk::operation::Manager> const &)) &smtk::session::aeva::CellSelection::create, py::arg("parent"), py::arg("selectedCells"), py::arg("arg2"))
    .def_static("create", (std::shared_ptr<smtk::session::aeva::CellSelection> (*)(::std::shared_ptr<smtk::session::aeva::Resource> const &, ::std::set<long long> const &, ::std::weak_ptr<smtk::operation::Manager> const &)) &smtk::session::aeva::CellSelection::create, py::arg("parent"), py::arg("cellIdsIn"), py::arg("arg2"))
    .def_static("instance", &smtk::session::aeva::CellSelection::instance)
    .def_readonly_static("type_name", &smtk::session::aeva::CellSelection::type_name)
    ;
  return instance;
}

#endif
