//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_ApplicationConfiguration_h
#define smtk_session_aeva_ApplicationConfiguration_h

#include "smtk/extension/paraview/appcomponents/ApplicationConfiguration.h"

/**\brief Configure aeva's operation panel.
  */
class ApplicationConfiguration
  : public QObject
  , public smtk::paraview::ApplicationConfiguration

{
  Q_OBJECT
  Q_INTERFACES(smtk::paraview::ApplicationConfiguration)

public:
  ApplicationConfiguration(QObject* parent);
  ~ApplicationConfiguration() override = default;

  /**\brief Provide configuration information for panels.
    *
    */
  smtk::view::Information panelConfiguration(const QDockWidget* panel) override;
};

#endif // smtk_extension_paraview_appcomponents_ApplicationConfiguration_h
