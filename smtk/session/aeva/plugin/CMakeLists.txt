set(headers)
set(interfaces
  AEVAContextMenu
  ApplicationConfiguration
  pqAEVAShortcuts
)
set(sources)

set(classes
  AEVAContextMenu
  ApplicationConfiguration
  EditFreeformAttributesView
  Registrar
  pqAEVAShortcuts
  pqAEVASurfaceFeatureToolBar
  pqSMTKProportionalEditItemWidget
  pqProportionalEditPropertyWidget
  pqSMTKVolumeInspectionItemWidget
  pqVolumeInspectionPropertyWidget
  pqAEVAWidgetsAutoStart
)

foreach(class ${classes})
  list(APPEND sources ${class}.cxx)
  list(APPEND headers ${class}.h)
endforeach()

set(ui_files
  resources/pqProportionalEditPropertyWidget.ui
  resources/pqVolumeInspectionPropertyWidget.ui
  resources/EditFreeformAttributesView.ui
)
set(CMAKE_AUTOUIC_SEARCH_PATHS
  "${CMAKE_CURRENT_SOURCE_DIR}/resources")

set_property(SOURCE smtkAEVASessionPlugin_qch.h PROPERTY SKIP_AUTOGEN ON)

paraview_plugin_add_toolbar(
  CLASS_NAME pqAEVASurfaceFeatureToolBar
  INTERFACES action_group_interfaces
  SOURCES action_group_sources
)

list(APPEND interfaces ${action_group_interfaces})
list(APPEND sources ${action_group_sources})

paraview_plugin_add_auto_start(
  CLASS_NAME pqAEVAWidgetsAutoStart
  INTERFACES auto_start_interfaces
  SOURCES auto_start_sources)
list(APPEND interfaces
  ${auto_start_interfaces})
list(APPEND sources
  ${auto_start_sources})

smtk_add_plugin(
  smtkAEVASessionPlugin
  REGISTRARS
    smtk::session::aeva::Registrar
    smtk::session::aeva::plugin::Registrar
  MANAGERS
    smtk::common::Managers
    smtk::operation::Manager
    smtk::resource::Manager
    smtk::view::Manager
  PARAVIEW_PLUGIN_ARGS
    VERSION "1.0"
    REQUIRED_ON_SERVER
    REQUIRED_ON_CLIENT
    SERVER_MANAGER_XML smconfig.xml
    UI_INTERFACES ${interfaces}
    UI_FILES ${ui_files}
    SOURCES ${sources}
    MODULES VTK::AEVAExt
)
target_link_libraries(smtkAEVASessionPlugin
  LINK_PUBLIC
    smtkAEVASession
  LINK_PRIVATE
    ParaView::pqApplicationComponents
    ParaView::pqCore
    smtkPQComponentsExt
    smtkPQWidgetsExt
    VTK::FiltersCore
    VTK::FiltersGeometry
)
# XXX(smtk): SMTK headers are not ready for this.
#target_compile_definitions(smtkAEVASessionPlugin
#  PRIVATE
#    QT_NO_KEYWORDS)
