//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/aeva/plugin/ApplicationConfiguration.h"

#include "smtk/session/aeva/OperationDecorator.h"

#include "smtk/extension/paraview/appcomponents/pqSMTKOperationToolboxPanel.h"
#include "smtk/view/Configuration.h"
#include "smtk/view/Information.h"
#include "smtk/view/json/jsonView.h"

ApplicationConfiguration::ApplicationConfiguration(QObject* parent)
  : QObject(parent)
{
}

smtk::view::Information ApplicationConfiguration::panelConfiguration(const QDockWidget* panel)
{
  smtk::view::Information result;
  if (const auto* toolbox = dynamic_cast<const pqSMTKOperationToolboxPanel*>(panel))
  {
    nlohmann::json jsonConfig = { { "Name", "Operations" },
      { "Type", "qtOperationPalette" },
      { "Component",
        { { "Name", "Details" },
          { "Attributes",
            { { "SearchBar", true }, { "Title", "Tools" }, { "SubsetSort", "Precedence" } } },
          { "Children",
            { { { "Name", "Model" }, { "Attributes", { { "Autorun", "true" } } } } } } } } };
    std::shared_ptr<smtk::view::Configuration> viewConfig = jsonConfig;
    result.insert_or_assign(viewConfig);

    auto operations = std::make_shared<smtk::session::aeva::OperationDecorator>();
    // Ensure we insert the decorator as a base-type shared-pointer
    // so it can be retrieved properly:
    result.insert_or_assign(std::dynamic_pointer_cast<smtk::view::OperationDecorator>(operations));
  }
  else
  {
    smtkWarningMacro(smtk::io::Logger::instance(),
      "Unknown panel named \"" << (panel ? panel->objectName().toStdString() : "null") << "\"\n");
  }
  return result;
}
