<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "volume inspect" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="volume inspect" Label="image inspector" BaseType="operation">
      <BriefDescription>Render i-, j-, and k-slices of volumetric image data.</BriefDescription>
      <DetailedDescription>
        This tool draws image-axis-aligned slice planes colored by scalar
        data present at each voxel. The planes can be moved in the 3-D view
        by clicking and dragging. Planes can also be entered into the operation
        parameter panel.
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <BriefDescription>The input volume </BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="volume"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Group Name="3D slice planes" Label="slice planes" NumberOfGroups="1">
          <ItemDefinitions>
            <Double Name="origin point" Label="origin point" NumberOfRequiredValues="3">
              <BriefDescription>Origin point of 3 planes</BriefDescription>
              <DefaultValue>0, 0, 0</DefaultValue>
              <ComponentLabels>
                <Label>X</Label>
                <Label>Y</Label>
                <Label>Z</Label>
              </ComponentLabels>
            </Double>
          </ItemDefinitions>
        </Group>
        <!-- Dummy input parameter not shown to user, so that ableToOperate() returns false. -->
        <String Name="ensure gui" NumberOfRequiredValues="1" AdvanceLevel="1" />
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(volume inspect)" BaseType="result"/>
  </Definitions>
  <Views>
    <View
      Type="Operation"
      Title="image inspector"
      TopLevel="true"
      FilterByAdvanceLevel="false"
      FilterByCategoryMode="false"
    >
      <InstancedAttributes>
        <Att Type="volume inspect">
          <ItemViews>
            <View Item="3D slice planes"
              Type="VolumeInspectionPlanes"
              OriginPoint="origin point"
              ShowControls="true"/>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
