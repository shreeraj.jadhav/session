//=========================================================================
//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/aeva/operators/ExportFebModel.h"
#include "smtk/session/aeva/ExportFebModel_xml.h"

namespace smtk
{
namespace session
{
namespace aeva
{

const char* ExportFebModel::xmlDescription() const
{
  return ExportFebModel_xml;
}

}
}
}
