<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "normal feature" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>

    <AttDef Type="unreferenced primitives feature" Label="unreferenced primitives" BaseType="operation">

      <BriefDescription>Select primitives not referenced by any side set.</BriefDescription>
      <DetailedDescription>
        Select primitives not referenced by any side set.
      </DetailedDescription>

      <AssociationsDef Name="model" NumberOfRequiredValues="1">
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="model"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <Int Name="primitive type">
          <BriefDescription>Choose the dimension of unreferenced primitives that should be selected.</BriefDescription>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="volume">3</Value>
            <Value Enum="surface">2</Value>
            <!-- TODO: aeva doesn't support model edges yet. <Value Enum="edge">1</Value> -->
            <!-- TODO: aeva doesn't support this yet. Value Enum="point">0</Value> -->
          </DiscreteInfo>
        </Int>
      </ItemDefinitions>

    </AttDef>

    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>

    <AttDef Type="result(unreferenced primitives feature)" BaseType="result">

      <ItemDefinitions>
        <Void Name="allow camera reset" IsEnabledByDefault="true" AdvanceLevel="11"/>
      </ItemDefinitions>

    </AttDef>
  </Definitions>
  <Views>
    <View Type="Operation" Title="unreferenced primitives" TopLevel="true" UseSelectionManager="true">
      <InstancedAttributes>
        <Att Type="unreferenced primitives feature">
          <ItemViews>
            <View Path="/model" Type="qtReferenceTree">
              <PhraseModel Type="smtk::view::ResourcePhraseModel">
                <SubphraseGenerator Type="smtk::view::SubphraseGenerator"/>
                <Badges>
                  <Badge
                    Type="smtk::extension::qt::MembershipBadge"
                    MembershipCriteria="Components"
                    Filter="model"
                    Default="false"/>
                  <Badge
                    Type="smtk::extension::paraview::appcomponents::VisibilityBadge"
                    Default="false"/>
                </Badges>
              </PhraseModel>
            </View>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
