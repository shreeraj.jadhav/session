<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the AEVA session "write" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="write" Label="write aeva data" BaseType="operation">
      <AssociationsDef LockType="Read" OnlyResources="true">
          <Accepts><Resource Name="smtk::session::aeva::Resource"/></Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <Void Name="copy all data with resource" Label="copy all data" Optional="true" IsEnabledByDefault="true" AdvanceLevel="1">
          <BriefDescription>
            Should geometric data in this file be copied to the output resource's folder?
            Without this option enabled, the output resource will reference existing files
            elsewhere on the filesystem.
          </BriefDescription>
        </Void>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(write)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
