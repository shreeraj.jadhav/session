//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/aeva/OperationDecorator.h"

#include "smtk/session/aeva/operators/AdjacencyFeature.h"
#include "smtk/session/aeva/operators/AllPrimitivesFeature.h"
#include "smtk/session/aeva/operators/Boolean.h"
#include "smtk/session/aeva/operators/BooleanIntersect.h"
#include "smtk/session/aeva/operators/BooleanSubtract.h"
#include "smtk/session/aeva/operators/BooleanUnite.h"
#include "smtk/session/aeva/operators/ChartGeneration.h"
#include "smtk/session/aeva/operators/CreateAnnotationResource.h"
#include "smtk/session/aeva/operators/Delete.h"
#include "smtk/session/aeva/operators/Duplicate.h"
#include "smtk/session/aeva/operators/EditFreeformAttributes.h"
#include "smtk/session/aeva/operators/Export.h"
#include "smtk/session/aeva/operators/ExportFeb.h"
#include "smtk/session/aeva/operators/ExportFebModel.h"
#include "smtk/session/aeva/operators/ExportModel.h"
#include "smtk/session/aeva/operators/GrowSelection.h"
#include "smtk/session/aeva/operators/Import.h"
#include "smtk/session/aeva/operators/ImprintGeometry.h"
#include "smtk/session/aeva/operators/ImprintImage.h"
#include "smtk/session/aeva/operators/LinearToQuadratic.h"
#include "smtk/session/aeva/operators/NetgenSurfaceRemesher.h"
#include "smtk/session/aeva/operators/NormalFeature.h"
#include "smtk/session/aeva/operators/PointsOfPrimitivesFeature.h"
#include "smtk/session/aeva/operators/ProportionalEdit.h"
#include "smtk/session/aeva/operators/ProximityFeature.h"
#include "smtk/session/aeva/operators/Read.h"
#include "smtk/session/aeva/operators/ReconstructSurface.h"
#include "smtk/session/aeva/operators/SmoothSurface.h"
#include "smtk/session/aeva/operators/TextureAtlas.h"
#include "smtk/session/aeva/operators/UnreferencedPrimitives.h"
#include "smtk/session/aeva/operators/VolumeInspect.h"
#include "smtk/session/aeva/operators/VolumeMesher.h"
#include "smtk/session/aeva/operators/Write.h"

#include "smtk/extension/vtk/operators/ImageInspector.h"
#include "smtk/extension/vtk/operators/MeshInspector.h"

#include "smtk/model/operators/AssignColors.h"

using namespace smtk::view;

namespace smtk
{
namespace session
{
namespace aeva
{

OperationDecorator::OperationDecorator()
  : smtk::view::OperationDecorator({ // !!
      // !! NB: Order matters; the precendence column indicates the order here:
      // !!
      // Inspection "operations"
      wrap<smtk::geometry::MeshInspector>("crinkle inspector",
        "Use a crinkle-slice to inspect the interior of a mesh.",
        {}),
      wrap<smtk::geometry::ImageInspector>("slice inspector",
        "Use a slice-plane to inspect the interior of an image or mesh.",
        {}),
      wrap<smtk::session::aeva::VolumeInspect>(),
      // Selection operations
      wrap<smtk::session::aeva::AllPrimitivesFeature>(),
      wrap<smtk::session::aeva::PointsOfPrimitivesFeature>(),
      wrap<smtk::session::aeva::GrowSelection>(),
      wrap<smtk::session::aeva::NormalFeature>(),
      wrap<smtk::session::aeva::ProximityFeature>(),
      wrap<smtk::session::aeva::AdjacencyFeature>(),
      wrap<smtk::session::aeva::UnreferencedPrimitives>(),
      wrap<smtk::session::aeva::BooleanIntersect>(),
      wrap<smtk::session::aeva::BooleanSubtract>(),
      wrap<smtk::session::aeva::BooleanUnite>(),
      wrap<smtk::session::aeva::Boolean>(),
      // Geometric operations
      wrap<smtk::session::aeva::SmoothSurface>(),
      wrap<smtk::session::aeva::NetgenSurfaceRemesher>(),
      wrap<smtk::session::aeva::ReconstructSurface>(),
      wrap<smtk::session::aeva::VolumeMesher>(),
      wrap<smtk::session::aeva::LinearToQuadratic>(),
      wrap<smtk::session::aeva::TextureAtlas>(),
      wrap<smtk::session::aeva::ChartGeneration>(),
      // Annotation transfer operations
      wrap<smtk::session::aeva::ImprintGeometry>(),
      wrap<smtk::session::aeva::ImprintImage>(),
      // Modeling operations
      wrap<smtk::session::aeva::Duplicate>(),
      wrap<smtk::session::aeva::EditFreeformAttributes>(),
      wrap<smtk::session::aeva::ProportionalEdit>(),
      wrap<smtk::session::aeva::Delete>(),
      // I/O operations
      wrap<smtk::session::aeva::ExportFebModel>("export to febio",
        "Export data in FeBio format",
        {}),
      wrap<smtk::session::aeva::ExportModel>("export to med", "Export data in MED format", {}),
      wrap<smtk::session::aeva::Import>("import into aeva",
        "Import data into aeva from a variety of formats.",
        {}),
      // Miscellany
      wrap<smtk::session::aeva::CreateAnnotationResource>(),
      wrap<smtk::model::AssignColors>("assign colors",
        "Assign colors to components.",
        {},
        "assign\ncolors") })
{
}

} // namespace aeva
} //namespace session
} // namespace smtk
