//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_session_aeva_json_jsonResource_h
#define smtk_session_aeva_json_jsonResource_h

#include "smtk/session/aeva/Exports.h"

#include "smtk/session/aeva/Resource.h"

#include "smtk/model/json/jsonResource.h"

#include "nlohmann/json.hpp"

// Define how aeva resources are serialized.
namespace smtk
{
namespace session
{
namespace aeva
{
using json = nlohmann::json;
SMTKAEVASESSION_EXPORT void to_json(json& j, const smtk::session::aeva::Resource::Ptr& resource);

SMTKAEVASESSION_EXPORT void from_json(const json& j, smtk::session::aeva::Resource::Ptr& resource);
}
}
}

#endif
