cmake_minimum_required(VERSION 3.12)

# Project name and version
include(cmake/aevaVersion.cmake)
project(aeva-session VERSION ${aeva_VERSION})

# Add our cmake directory to the module search path
list(INSERT CMAKE_MODULE_PATH 0
  ${PROJECT_SOURCE_DIR}/cmake)

# This plugin requires C++11
if(NOT DEFINED CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 11)
  set(CMAKE_CXX_STANDARD_REQUIRED True)
  set(CMAKE_CXX_EXTENSIONS False)
endif()

# Use shared libs, otherwise we don't get a loadable plugin
set(BUILD_SHARED_LIBS True)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

include(aevaCoverage)
include(aevaOptions)
include(aevaDependencies)
include(aevaSanitize)
include(aevaSettings)
include(aevaEarlyVTKModuleScan) # For modules without smtkAEVASession dependencies

add_subdirectory(smtk)
add_subdirectory(data)

include(aevaPackaging)
