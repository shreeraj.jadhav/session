# Setting this ensures that "make install" will leave rpaths to external
# libraries (not part of the build-tree e.g. Qt, ffmpeg, etc.) intact on
# "make install". This ensures that one can install a version of ParaView on the
# build machine without any issues. If this not desired, simply specify
# CMAKE_INSTALL_RPATH_USE_LINK_PATH when configuring and "make install" will
# strip all rpaths, which is default behavior.
if (NOT DEFINED CMAKE_INSTALL_RPATH_USE_LINK_PATH)
  set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
endif ()

# Use GNU install directories.
include(GNUInstallDirs)
set(CMAKE_INSTALL_CMAKEDIR "${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}-${PROJECT_VERSION}")

# Set the directory where the binaries will be stored
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_CMAKE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_CMAKEDIR})

if (AEVA_ENABLE_PYTHON)
  if(WIN32)
    set(PYTHON_MODULE_EXTENSION ".pyd")
  else()
    set(PYTHON_MODULE_EXTENSION ".so")
  endif()

  # Initialize AEVA_PYTHON_MODULEDIR.
  # This stores the location where we'll install AEVA's Python modules.
  # Note that AEVA_PYTHON_MODULEDIR may be provided if AEVA is being
  # built as a submodule or as part of a superbuild.
  if (NOT DEFINED AEVA_PYTHON_MODULEDIR)
    if (AEVA_INSTALL_PYTHON_TO_SITE_PACKAGES)
      execute_process(
        COMMAND
          "$<TARGET_FILE:Python3::Interpreter>"
          -c "import site; print(site.getsitepackages())[-1]"
        RESULT_VARIABLE AEVA_PYTHON_MODULEDIR
      )
    elseif(WIN32)
      set(AEVA_PYTHON_MODULEDIR "bin/Lib/site-packages")
    else()
      set(AEVA_PYTHON_MODULEDIR
        "${CMAKE_INSTALL_LIBDIR}/python${Python3_VERSION_MAJOR}.${Python3_VERSION_MINOR}/site-packages")
    endif()
  endif()

  # Set up environment variables needed to import the python modules for tests.
  if (PYTHONINTERP_FOUND AND AEVA_ENABLE_TESTING)
    set(required_python_modules)

    # Add the path to the build tree's compiled modules, prioritizing it over
    # the other directories (since the other directories will likely include the
    # location of the module's install).
    set(aeva_pythonpaths
      "${CMAKE_BINARY_DIR}/${AEVA_PYTHON_MODULEDIR}")
    set(aeva_libpaths
      "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR}")

    list(APPEND aeva_pythonpaths "${ParaView_PREFIX_PATH}/${PARAVIEW_PYTHONPATH}")
    list(APPEND aeva_libpaths "${ParaView_PREFIX_PATH}/${CMAKE_INSTALL_BINDIR}")

    set(envsep ":")
    if (WIN32)
      set(envsep ";")
    endif ()

    set(libpath_envvar)
    if (WIN32)
      list(INSERT aeva_libpaths 0 $ENV{PATH})
      set(libpath_envvar "PATH")
    endif ()

    # Remove duplicates.
    if (aeva_pythonpaths)
      list(REMOVE_DUPLICATES aeva_pythonpaths)
    endif ()
    if (aeva_libpaths)
      list(REMOVE_DUPLICATES aeva_libpaths)
    endif ()

    # Append paths as needed.
    list(APPEND aeva_pythonpaths "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")
    if (libpath_envvar)
      string(APPEND aeva_libpath_env "${envsep}$ENV{${libpath_envvar}}")
    endif ()

    string(REPLACE ";" "${envsep}" aeva_pythonpath_env "${aeva_pythonpaths}")
    string(REPLACE ";" "${envsep}" aeva_libpath_env "${aeva_libpaths}")
    if (WIN32)
      string(REPLACE "/" "\\" aeva_libpath_env "${aeva_libpath_env}")
    endif ()
    string(REPLACE ";" "\;" aeva_pythonpath_env "${aeva_pythonpath_env}")
    string(REPLACE ";" "\;" aeva_libpath_env "${aeva_libpath_env}")
    set(aeva_python_test_environment
      "PYTHONPATH=${aeva_pythonpath_env}")

    if (libpath_envvar AND aeva_libpath_env)
      list(APPEND aeva_python_test_environment
        "${libpath_envvar}=${aeva_libpath_env}")
    endif ()

    function (aeva_add_test_python name file)
      if (NOT IS_ABSOLUTE "${file}")
        set(file "${CMAKE_CURRENT_SOURCE_DIR}/${file}")
      endif ()
      add_test(
        NAME    "${name}"
        COMMAND "$<TARGET_FILE:Python3::Interpreter>" "${file}"
                ${ARGN})
      set_tests_properties("${name}"
        PROPERTIES
          ENVIRONMENT "${aeva_python_test_environment}")
      aeva_test_apply_sanitizer("${name}")
    endfunction ()
  endif()

endif()

if (AEVA_ENABLE_TESTING)
  include(CTest)
  enable_testing()
  include(TestingMacros)

  # Do not report some warnings from generated code to the dashboard:
  configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/cmake/CTestCustom.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/CTestCustom.cmake")
endif()
