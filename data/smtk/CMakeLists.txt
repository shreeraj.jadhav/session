set(data
  2834f8a9-83d5-48ec-a0f8-868abf06c6fe.vtu
  56191b04-6fb3-4230-ac59-b94309a15c02.vtu
  c87fc298-ebf6-4e8e-a53e-6251e4f9c3eb.vtu
  df883849-e8a8-4389-8feb-7ac57bc6f8f2.vtp
  e5c55489-a607-4a9d-a996-f32a7e9ec940.vtu
  simple.smtk
  tri-a.vtk
)

# Install sample data for use by the aeva application
smtk_get_kit_name(name dir_prefix)
install(
  FILES
    ${data}
  DESTINATION
    share/${PROJECT_NAME}/${PROJECT_VERSION}/${dir_prefix}
)
